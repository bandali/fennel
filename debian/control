Source: fennel
Section: devel
Priority: optional
Maintainer: Phil Hagelberg <phil@hagelb.org>
Uploaders: Amin Bandali <bandali@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 lua5.4,
Standards-Version: 4.6.2
Homepage: https://fennel-lang.org
Vcs-Browser: https://git.sr.ht/~technomancy/fennel
Vcs-Git: https://git.sr.ht/~technomancy/fennel
Rules-Requires-Root: no

Package: fennel
Architecture: all
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 lua5.4 | lua,
Description: Lisp-based programming language for the Lua runtime
 Fennel is a lisp that compiles to Lua. It aims to be easy to use,
 expressive, and has almost zero overhead compared to handwritten Lua.
 .
 You can use any function or library from Lua in Fennel, and vice versa.
 Compiled code should be just as or more efficient than hand-written Lua.
 Macros allow you to ship compiled code with no runtime dependency on Fennel.
 Fennel is a one-file library as well as an executable. Embed it in other
 programs to support runtime extensibility and interactive development.
